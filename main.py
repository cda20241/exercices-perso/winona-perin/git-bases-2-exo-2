from somme import somme, somme3nombres
from soustraction import soustraction

if __name__ == '__main__':
    print("hello world")
    print("la somme de 2 et 6.2 vaut : ", somme(2, 6.2))
    print("la soustraction de 5 et 2 vaut : ", soustraction(5, 2))
    print("la somme de 2 et 6.2 et 10 vaut : ", somme3nombres(2, 6.2, 10))
