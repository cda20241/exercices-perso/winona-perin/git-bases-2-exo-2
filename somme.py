def somme(a: float, b: float)\
        -> float:
    return a + b

def somme3nombres(a: float, b: float, c: float)\
        -> float:
    return a + b + c
